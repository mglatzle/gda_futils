from gda_futils import gda_futils
import numpy as np
from functools import wraps
import gc
import timeit


def MeasureTime(f, n=1):
    """
    Wraps a function to time it.

    Arguments
    ---------
    f : callable
        The callable to time.
    n : integer
        Number of executions to perform.

    Returns
    -------
     : float
        Execution time [s] averaged of `n` executions.
     : list
        The return value for each execution.

    References
    ----------
    .. [sx7370801] https://stackoverflow.com/a/52288622/3465171
    """
    @wraps(f)
    def _wrapper(*args, **kwargs):
        result = []
        times = np.zeros(n)
        for i in range(n):
            gcold = gc.isenabled()
            gc.disable()
            try:
                start_time = timeit.default_timer()
                r = f(*args, **kwargs)
                elapsed = timeit.default_timer() - start_time
            finally:
                if gcold:
                    gc.enable()
            result.append(r)
            times[i] = elapsed
        return np.mean(times), result
    return _wrapper


def profile(origin, source_pos, sigma_source_parallax, grid_shape, cell_size,
            sf_x, sf_y, max_n_threads=32, n_runs=10):
    if gda_futils.sf_x is not None:
        raise RuntimeError("sf_x already allocated!")
    if gda_futils.sf_y is not None:
        raise RuntimeError("sf_y already allocated!")
    try:
        gda_futils.init_survival_function(
            sf_x,
            sf_y
        )
        tau_per_l = np.ones(grid_shape)
        response = np.zeros(source_pos.shape[1])

        res = MeasureTime(gda_futils.compute_response, n=n_runs)
        adj = MeasureTime(gda_futils.compute_adjoint_response, n=n_runs)

        res_times = np.empty(max_n_threads)
        adj_times = np.empty(max_n_threads)
        for f, t in zip([res, adj], [res_times, adj_times]):
            for n in range(max_n_threads):
                time, result = f(
                    origin, source_pos, sigma_source_parallax, tau_per_l.ravel(),
                    tau_per_l.shape, cell_size, response, n_threads=n+1
                )
                r = np.array(result)
                assert np.all(r == 0), "Got non-zero error!"
                t[n] = time
    finally:
        gda_futils.terminate_survival_function()
    return res_times, adj_times


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    grid_shape = 256*np.ones(3, dtype=np.int64)
    cell_size = 600/256
    origin = grid_shape/2 * cell_size

    source_pos = np.load(
        '/afs/mpa/temp/reimar/Gaia/full_data/data_for_mglatzle/selected_star_positions.npy'
    )
    source_radii = np.sqrt(np.sum((source_pos-origin)**2, axis=1))

    source_pos = (source_pos.T).reshape((3, -1), order='F')
    sf_x = np.load(
        '/afs/mpa/temp/reimar/Gaia/full_data/data_for_mglatzle/sf_x.npy'
    )
    sf_y = np.load(
        '/afs/mpa/temp/reimar/Gaia/full_data/data_for_mglatzle/sf_y.npy'
    )
    max_n_threads = 32
    n_runs = 5
    sigma_source_parallax = np.load(
        '/afs/mpa/temp/reimar/Gaia/full_data/data_for_mglatzle/selected_parallax_err.npy'
    )

    t_r, t_a = profile(origin, source_pos, sigma_source_parallax, grid_shape,
                       cell_size, sf_x, sf_y, max_n_threads=max_n_threads,
                       n_runs=n_runs)
    fig, ax = plt.subplots()
    x = np.array([i for i in range(1, max_n_threads+1)])
    ax.plot(x, t_r, label='response')
    ax.plot(x, t_a, label='adjoint')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel(r'# threads')
    ax.set_ylabel(r'time [s]')
    ax.legend()
    fig.tight_layout()
    fig.savefig('timings.png')
