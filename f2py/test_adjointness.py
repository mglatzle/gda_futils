from gda_futils import gda_futils
import numpy as np
np.random.seed(42)

###############################################################################
cube_size = 8
N_lines = int(2e2)
grid_shape = cube_size*np.ones(3, dtype=np.int64)
cell_size = 1
n_threads = 16
origin = grid_shape/2 * cell_size
source_pos = cube_size * np.random.rand(3*N_lines).reshape(
    (3, N_lines), order='F'
)
sigma_source_parallax = np.zeros(N_lines)
###############################################################################

###############################################################################
for i in range(10):
    s = np.random.randn(np.prod(grid_shape))
    d = np.random.randn(source_pos.shape[1])
    Rs = np.zeros_like(d)
    Rad = np.zeros_like(s)
    err1 = gda_futils.compute_response(
        origin, source_pos, sigma_source_parallax, s,
        grid_shape, cell_size, Rs, n_threads=n_threads
    )
    err2 = gda_futils.compute_adjoint_response(
        origin, source_pos, sigma_source_parallax, Rad,
        grid_shape, cell_size, d, n_threads=n_threads
    )

    assert err1 == 0, "Unexpected error value returned."
    assert err2 == 0, "Unexpected error value returned."
    assert \
        np.abs(np.dot(Rad, s) - np.dot(d, Rs)) < 1e-12*np.sqrt(cube_size**3), \
        "Adjointness test failed"
###############################################################################
