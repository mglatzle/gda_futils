from gda_futils import gda_futils
import numpy as np

###############################################################################
grid_shape = 2*np.ones(3, dtype=np.int64)
cell_size = 1
origin = grid_shape/2 * cell_size
source_pos = np.array(
    [0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1.5, 1.5]
).reshape((3, 3), order='F')
tau_per_l = np.zeros(np.prod(grid_shape), dtype=np.float64).reshape(grid_shape)
response = np.zeros(source_pos.shape[1])
sigma_source_parallax = np.zeros(source_pos.shape[1])
###############################################################################

###############################################################################
# reproduce test_compute_response_std from test_gda_futils.f90

tau_per_l[0, 0, 0] = 0.5
tau_per_l[1, 0, 0] = 0.3
tau_per_l[0, 1, 1] = 0.9
error = gda_futils.compute_response(
    origin, source_pos, sigma_source_parallax, tau_per_l.ravel(),
    grid_shape, cell_size, response
)

assert error == 0, "Unexpected error value returned."
assert np.allclose(
    0.5*np.sqrt(3)*np.array([0.5, 0.3, 0.9]),
    response,
    rtol=1e-6,
    atol=0
), "Unexpected response returned."
###############################################################################

###############################################################################
# reproduce test_compute_adjoint_response_std from test_gda_futils.f90

tau_per_l[:] = 0
response = np.array([1., 2., 3.])
error = gda_futils.compute_adjoint_response(
    origin, source_pos, sigma_source_parallax, tau_per_l.ravel(),
    grid_shape, cell_size, response
)

assert error == 0, "Unexpected error value returned."
assert np.allclose(
    0.5*np.sqrt(3)*np.array(
        [
            [
                [1.0, 0.0],
                [0.0, 3.0],
            ],
            [
                [2.0, 0.0],
                [0.0, 0.0]
            ]
        ]
    ),
    tau_per_l,
    rtol=1e-6,
    atol=1e-14
), "Unexpected tau_per_l returned."
###############################################################################

###############################################################################
# reproduce test_compute_response_sigma from test_gda_futils.f90

response[:] = 0
tau_per_l[:] = 0
tau_per_l[0, 0, 0] = 0.5
tau_per_l[1, 0, 0] = 0.3
tau_per_l[0, 1, 1] = 0.9
sigma_source_parallax[:] = .1
new_length = 1./(1/(.5*np.sqrt(3))-3*.1)
gda_futils.init_survival_function(
    np.linspace(-3, 3, 200),
    np.ones(200)
)
error = gda_futils.compute_response(
    origin, source_pos, sigma_source_parallax, tau_per_l.ravel(),
    grid_shape, cell_size, response
)
gda_futils.terminate_survival_function()

assert error == 0, "Unexpected error value returned."
assert np.allclose(
    new_length*np.array([0.5, 0.3, 0.9]),
    response,
    rtol=1e-6,
    atol=0
), "Unexpected response returned."
###############################################################################

###############################################################################
# reproduce test_compute_adjoint_response_sigma from test_gda_futils.f90

tau_per_l[:] = 0
response = np.array([1., 2., 3.])
sigma_source_parallax[:] = .1
new_length = 1./(1/(.5*np.sqrt(3))-3*.1)
gda_futils.init_survival_function(
    np.linspace(-3, 3, 200),
    np.ones(200)
)
error = gda_futils.compute_adjoint_response(
    origin, source_pos, sigma_source_parallax, tau_per_l.ravel(),
    grid_shape, cell_size, response
)
gda_futils.terminate_survival_function()

assert error == 0, "Unexpected error value returned."
assert np.allclose(
    new_length*np.array(
        [
            [
                [1.0, 0.0],
                [0.0, 3.0],
            ],
            [
                [2.0, 0.0],
                [0.0, 0.0]
            ]
        ]
    ),
    tau_per_l,
    rtol=1e-6,
    atol=1e-14
), "Unexpected tau_per_l returned."
###############################################################################
