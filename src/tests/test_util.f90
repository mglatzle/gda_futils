module test_util
  use fruit
  use util
#ifdef _OPENMP
  use omp_lib
#endif
  implicit none
contains

  subroutine test_ravel_index_3D_Cartesian_C_order()
    integer, dimension(3) :: ijk, shp
    integer :: idx

    ijk = [1, 1, 1]
    shp = [128, 128, 128]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(1, idx)

    ijk = [1, 1, 2]
    shp = [128, 128, 128]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(2, idx)

    ijk = [128, 128, 128]
    shp = [128, 128, 128]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(128**3, idx)

    ijk = [10, 5, 10]
    shp = [10, 5, 10]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(500, idx)

    ijk = [1, 2, 1]
    shp = [3, 5, 10]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(11, idx)

    ijk = [2, 1, 1]
    shp = [3, 5, 10]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(51, idx)

    ijk = [2, 1, 2]
    shp = [3, 5, 10]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(52, idx)

    ijk = [2, 2, 1]
    shp = [3, 5, 10]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(61, idx)

    ijk = [10, 2, 1]
    shp = [10, 5, 10]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(450+10+1, idx)

    ijk = [2, 3, 4]
    shp = [3, 5, 10]
    idx = ravel_index_3D_Cartesian_C_order(ijk, shp)
    call assert_equals(50+20+4, idx)
  end subroutine test_ravel_index_3D_Cartesian_C_order

end module test_util
