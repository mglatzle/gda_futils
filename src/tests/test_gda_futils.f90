module test_gda_futils
  use portability
  use fruit
  use gda_futils
#ifdef _OPENMP
  use omp_lib
#endif
  implicit none

contains

  subroutine test_compute_response_std()
    real(fp) :: origin(3), cell_size
    integer :: grid_shape(3), n_sources, n_cells, n_threads, error
    real(fp), allocatable :: tau_per_l(:), source_pos(:, :), response(:), &
         sigma_source_parallax(:)

    error = 0

    grid_shape = [2, 2, 2]
    n_cells = product(grid_shape)

    n_sources = 3
    allocate(source_pos(3, n_sources))
    allocate(sigma_source_parallax(n_sources))
    sigma_source_parallax = 0

    ! 1D array that represents 3D array in C-order
    allocate(tau_per_l(n_cells))
    tau_per_l = 0

    ! for first source ijk=[1, 1, 1]
    tau_per_l(1) = 0.5_fp
    ! for second source ijk=[2, 1, 1]
    tau_per_l(5) = 0.3_fp
    ! for third source ijk=[1, 2, 2]
    tau_per_l(4) = 0.9_fp

    allocate(response(n_sources))
    response = 10
    n_threads = 3

    cell_size  = 1
    origin  = 0.5_fp*grid_shape*cell_size
    source_pos = cell_size * reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1.5, 1.5], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         cell_size*0.5_fp*sqrt(3._fp)*[real(fp) :: 0.5, 0.3, 0.9], response, &
         n_sources, 1e-6_fp, "cell size 1" &
         )

    cell_size  = 0.1_fp
    origin  = 0.5_fp*grid_shape*cell_size
    source_pos = cell_size * reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1.5, 1.5], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         cell_size*0.5_fp*sqrt(3._fp)*[real(fp) :: 0.5, 0.3, 0.9], response, &
         n_sources, 1e-6_fp, "cell size 0.1" &
         )

    cell_size  = 10
    origin  = 0.5_fp*grid_shape*cell_size
    source_pos = cell_size * reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1.5, 1.5], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         cell_size*0.5_fp*sqrt(3._fp)*[real(fp) :: 0.5, 0.3, 0.9], response, &
         n_sources, 1e-6_fp, "cell size 10" &
         )

    tau_per_l = 0
    ! ijk=[1, 1, 1]
    tau_per_l(1) = 1
    ! ijk=[1, 1, 2]
    tau_per_l(2) = 2
    cell_size  = 1
    origin  = [0.5_fp, 0.5_fp, 2.0_fp]
    source_pos = cell_size * reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 0.5, 0.5, 1.0, 0.5, 0.5, 1.5], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         cell_size*0.5_fp*[real(fp) :: 5, 4, 2], response, &
         n_sources, 1e-6_fp, "initial ijk different from [1, 1, 1]" &
         )
  end subroutine test_compute_response_std

  subroutine test_compute_response_sigma()
    real(fp) :: origin(3), cell_size
    integer :: grid_shape(3), n_sources, n_cells, n_threads, error, i
    real(fp), allocatable :: tau_per_l(:), source_pos(:, :), response(:), &
         sigma_source_parallax(:), x(:), y(:)

    allocate(x(200))
    allocate(y(200))
    x = [(-3._fp + 6._fp/200*i, i=1, 200)]
    y = 0.5_fp
    call init_survival_function(size(x), x, y)

    error = 0

    grid_shape = [2, 2, 2]
    cell_size  = 1

    n_cells = product(grid_shape)
    origin  = 0.5_fp*grid_shape*cell_size

    n_sources = 3
    allocate(source_pos(3, n_sources))
    source_pos = reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1.5, 1.5], &
         [3, n_sources])
    allocate(sigma_source_parallax(n_sources))
    sigma_source_parallax = 0.1_fp

    ! 1D array that represents 3D array in C-order
    allocate(tau_per_l(n_cells))
    tau_per_l = 0

    ! for first source ijk=[1, 1, 1]
    tau_per_l(1) = 0.5_fp
    ! for second source ijk=[2, 1, 1]
    tau_per_l(5) = 0.3_fp
    ! for third source ijk=[1, 2, 2]
    tau_per_l(4) = 0.9_fp

    allocate(response(n_sources))
    n_threads = 3
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )

    call assert_equals(0, error)

    call assert_equals( &
         0.5_fp/(1._fp/(.5_fp*sqrt(3._fp))-3*0.1_fp) * &
         [real(fp) :: 0.5, 0.3, 0.9], response, &
         n_sources, 1e-6_fp &
         )

    call terminate_survival_function()
  end subroutine test_compute_response_sigma

  subroutine test_compute_response_edge_cases()
    real(fp) :: origin(3), cell_size
    integer :: grid_shape(3), n_sources, n_cells, n_threads, error, i
    real(fp), allocatable :: tau_per_l(:), source_pos(:, :), response(:), &
         sigma_source_parallax(:), x(:), y(:)
    n_threads = 4

    allocate(x(200))
    allocate(y(200))
    x = [(-3._fp + 6._fp/200*i, i=1, 200)]
    y = 1
    call init_survival_function(size(x), x, y)

    error = 0

    grid_shape = [1, 1, 20]
    cell_size  = 1

    n_cells = product(grid_shape)

    ! 1D array that represents 3D array in C-order
    allocate(tau_per_l(n_cells))
    tau_per_l = 1

    n_sources = 1
    allocate(source_pos(3, n_sources))
    allocate(sigma_source_parallax(n_sources))
    sigma_source_parallax = 0
    allocate(response(n_sources))

    response = 0
    origin  = cell_size*[real(fp) :: 0.5, 0.5, tiny(1.0_fp)]
    source_pos = reshape( &
         cell_size*[real(fp) :: 0.5, 0.5, 20.0], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         [real(fp) :: 20.0], response, &
         n_sources, 1e-6_fp, &
         'Along positive z axis' &
         )

    response = 0
    origin  = cell_size*[real(fp) :: 0.5, 0.5, 20.0_fp-spacing(20.0_fp)]
    source_pos = reshape( &
         cell_size*[real(fp) :: 0.5, 0.5, 0.0], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         [real(fp) :: 20.0], response, &
         n_sources, 1e-6_fp, &
         'Along negative z axis' &
         )

    response = 0
    origin  = cell_size*[real(fp) :: 0.5, 0.5, 0.5]
    source_pos = reshape( &
         [real(fp) :: 0.5, 0.5, 0.5+spacing(0.5_fp)], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         [real(fp) :: 0.0], response, &
         n_sources, 1e-6_fp &
         )

    response = 0
    origin  = tiny(1.0_fp)
    source_pos = reshape( &
         [real(fp) :: tiny(1.0_fp), tiny(1.0_fp), 20.0], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         [real(fp) :: 20.0], response, &
         n_sources, 1e-6_fp, &
         'Along grid wall.' &
         )

    call terminate_survival_function()
  end subroutine test_compute_response_edge_cases

  subroutine test_compute_response_large_grid()
    real(fp) :: origin(3), cell_size
    integer :: grid_shape(3), n_sources, n_cells, n_threads, error
    real(fp), allocatable :: tau_per_l(:), source_pos(:, :), response(:), &
         sigma_source_parallax(:)

    error = 0

    grid_shape = [256, 256, 256]
    n_cells = product(grid_shape)

    n_sources = 3
    allocate(source_pos(3, n_sources))
    allocate(sigma_source_parallax(n_sources))
    sigma_source_parallax = 0

    ! 1D array that represents 3D array in C-order
    allocate(tau_per_l(n_cells))
    tau_per_l = 0
    tau_per_l(1:256) = 3.5_fp

    allocate(response(n_sources))
    n_threads = 3

    cell_size  = 2.5_fp
    origin  = cell_size*[real(fp) :: 0.5, 0.5, 0.5]
    source_pos = cell_size * reshape( &
         [real(fp) :: 0.5, 0.5, 256, 256, 0.5, 0.5, 255.5, 255.5, 255.5], &
         [3, n_sources])
    call compute_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         cell_size*3.5_fp*[real(fp) :: 255.5, 0.5, 0.5_fp*sqrt(3._fp)], &
         response, &
         n_sources, 1e-6_fp, "large grid" &
         )
  end subroutine test_compute_response_large_grid

  subroutine test_compute_adjoint_response_std()
    real(fp) :: origin(3), cell_size
    integer :: grid_shape(3), n_sources, n_cells, n_threads, error, i
    real(fp), allocatable :: tau_per_l(:), source_pos(:, :), response(:), &
         sigma_source_parallax(:)

    error = 0

    grid_shape = [2, 2, 2]
    cell_size  = 1

    n_cells = product(grid_shape)
    origin  = 0.5_fp*grid_shape*cell_size

    n_sources = 3
    allocate(source_pos(3, n_sources))
    source_pos = reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1.5, 1.5], &
         [3, n_sources])
    allocate(sigma_source_parallax(n_sources))
    sigma_source_parallax = 0

    ! 1D array that represents 3D array in C-order
    allocate(tau_per_l(n_cells))
    tau_per_l = 0

    allocate(response(n_sources))
    response = [real(fp) :: (i, i=1, 3)]

    n_threads = 3
    call compute_adjoint_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         0.5_fp*sqrt(3._fp)*response, &
         [tau_per_l(1), tau_per_l(5), tau_per_l(4)], &
         n_sources, 1e-6_fp &
         )
    call assert_equals( &
         [real(fp) :: (0, i=1, 5)], &
         [tau_per_l(2), tau_per_l(3), tau_per_l(6), &
         tau_per_l(7), tau_per_l(8)], &
         n_sources, 1e-6_fp &
         )

    origin  = [0.5_fp, 0.5_fp, 2.0_fp]
    source_pos = cell_size * reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 0.5, 0.5, 1.0, 0.5, 0.5, 1.5], &
         [3, n_sources])
    call compute_adjoint_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )
    call assert_equals(0, error)
    call assert_equals( &
         [0.5_fp*response(1), response(1)+response(2)+0.5_fp*response(3)], &
         [tau_per_l(1), tau_per_l(2)], &
         2, 1e-6_fp &
         )
    call assert_equals( &
         [real(fp) :: (0, i=1, 6)], &
         [tau_per_l(3), tau_per_l(4), tau_per_l(5), &
         tau_per_l(6), tau_per_l(7), tau_per_l(8)], &
         n_sources, 1e-6_fp &
         )
  end subroutine test_compute_adjoint_response_std

  subroutine test_compute_adjoint_response_sigma()
    real(fp) :: origin(3), cell_size
    integer :: grid_shape(3), n_sources, n_cells, n_threads, error, i
    real(fp), allocatable :: tau_per_l(:), source_pos(:, :), response(:), &
         sigma_source_parallax(:), x(:), y(:)

    allocate(x(200))
    allocate(y(200))
    x = [(-3._fp + 6._fp/200*i, i=1, 200)]
    y = 1
    call init_survival_function(size(x), x, y)

    error = 0

    grid_shape = [2, 2, 2]
    cell_size  = 1

    n_cells = product(grid_shape)
    origin  = 0.5_fp*grid_shape*cell_size

    n_sources = 3
    allocate(source_pos(3, n_sources))
    source_pos = reshape( &
         [real(fp) :: 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1.5, 1.5], &
         [3, n_sources])
    allocate(sigma_source_parallax(n_sources))
    sigma_source_parallax = 0.1_fp
    sigma_source_parallax(1) = 0.01_fp

    ! 1D array that represents 3D array in C-order
    allocate(tau_per_l(n_cells))
    tau_per_l = 0

    allocate(response(n_sources))
    response = [real(fp) :: (i, i=1, 3)]

    n_threads = 3
    call compute_adjoint_response( &
         n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
         tau_per_l, grid_shape, cell_size, response, n_threads, error &
         )

    call assert_equals(0, error)

    call assert_equals( &
         1._fp/(1._fp/(.5_fp*sqrt(3._fp))-3*sigma_source_parallax)*response, &
         [tau_per_l(1), tau_per_l(5), tau_per_l(4)], &
         n_sources, 1e-6_fp &
         )
    call assert_equals( &
         [real(fp) :: (0, i=1, 5)], &
         [tau_per_l(2), tau_per_l(3), tau_per_l(6), &
         tau_per_l(7), tau_per_l(8)], &
         n_sources, 1e-6_fp &
         )

    call terminate_survival_function()
  end subroutine test_compute_adjoint_response_sigma

  subroutine test_interpolate_survival_function
    real(fp) :: r, r_source, sigma, prob
    real(fp), allocatable :: x(:), y(:)
    integer :: start_idx, i, n_points

    n_points = 200
    allocate(x(n_points+1))
    allocate(y(n_points+1))


    r_source = 100
    sigma = 0
    r = 100
    start_idx = 1
    call interpolate_survival_function( &
         r, r_source, sigma, start_idx, prob &
         )
    call assert_equals(1._fp, prob, 1e-10_fp, "zero sigma, value")
    call assert_equals(1, start_idx, "zero sigma, index")

    r_source = 100
    sigma = 0.2_fp/r_source
    start_idx = 1

    x = [(-3._fp + i*1._fp/(n_points*sigma*r_source), i=0, n_points)]
    y = [(1._fp - i*1.0_fp/n_points, i=0, n_points)]
    call init_survival_function(size(x), x, y)

    r = 1
    prob = 0
    call interpolate_survival_function( &
         r, 1.0_fp/r_source, 1.0_fp/sigma, start_idx, prob &
         )
    call assert_equals(1.0_fp, prob, 1e-10_fp, "below range, value")
    call assert_equals(1, start_idx, "below range, index")

    r = r_source
    prob = 0
    call interpolate_survival_function( &
         r, 1.0_fp/r_source, 1.0_fp/sigma, start_idx, prob &
         )
    call assert_equals(2._fp/5, prob, 1e-10_fp, "mid range, value")
    call assert_equals(120, start_idx, "mid range, index")

    r = 50000
    prob = 0
    call interpolate_survival_function( &
         r, 1.0_fp/r_source, 1.0_fp/sigma, start_idx, prob &
         )
    call assert_equals(0._fp, prob, 1e-10_fp, "above range, value")
    call assert_equals(120, start_idx, "above range, index")

    call terminate_survival_function()
  end subroutine test_interpolate_survival_function
end module test_gda_futils
