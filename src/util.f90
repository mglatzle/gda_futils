module util
  use portability
  implicit none
  private
  public :: propagate_to_cell_interface_3D_Cartesian
  public :: ravel_index_3D_Cartesian_C_order
  public :: get_formatted_file_record_length
  public :: to_string_i4b

contains

  subroutine propagate_to_cell_interface_3D_Cartesian(pos, ijk, &
       direction, path)
    use, intrinsic :: ieee_arithmetic, only: ieee_value, ieee_positive_inf
    real(fp), intent(inout) :: pos(3)
    integer, intent(inout) :: ijk(3)
    real(fp), intent(in) :: direction(3)
    real(fp), intent(out) :: path

    integer :: m
    real(fp) :: displacement(3)


    ! Need to differentiate between going in positive or negative direction in
    ! each coordinate.
    do m=1, 3
       if(direction(m)>0) then
          displacement(m) = (ijk(m) - pos(m))/direction(m)
       else if(direction(m)<0) then
          displacement(m) = (ijk(m) - 1.0_fp - pos(m))/direction(m)
       else
          ! When direction vector is perpendicular to a coordinate, the
          ! displacement we need to change the corresponding index is infinite.
          displacement(m) = ieee_value(displacement(m), ieee_positive_inf)
       endif
    end do

    m = minloc(displacement, dim=1)
    path = displacement(m)

    ! Update the index or indices that allow for crossing over into next cell
    ! with shortest displacement.
    if(direction(m) > 0) then
       ijk(m) = ijk(m) + 1
    else
       ijk(m) = ijk(m) - 1
    end if
    pos = pos + path*direction
  end subroutine propagate_to_cell_interface_3D_Cartesian

  function ravel_index_3D_Cartesian(ijk, shp)
    integer, intent(in) :: ijk(3), shp(3)
    integer :: ravel_index_3D_Cartesian

    ravel_index_3D_Cartesian = &
         ijk(1) + shp(1)*(ijk(2)-1) + shp(1)*shp(2)*(ijk(3)-1)
  end function ravel_index_3D_Cartesian

  function ravel_index_3D_Cartesian_C_order(ijk, shp)
    integer, intent(in) :: ijk(3), shp(3)
    integer :: ravel_index_3D_Cartesian_C_order

    ravel_index_3D_Cartesian_C_order = &
         shp(3)*shp(2)*(ijk(1)-1) + shp(3)*(ijk(2)-1) + ijk(3)
  end function ravel_index_3D_Cartesian_C_order


  function get_formatted_file_record_length(file)
    character(:), allocatable, intent(in) :: file

    integer :: recl, unit

    integer :: get_formatted_file_record_length

    inquire(iolength=recl) get_formatted_file_record_length
    open(newunit=unit, file=file, status='old', action='read', &
         access='direct', form='unformatted', recl=recl)
    read(unit, rec=1) get_formatted_file_record_length
    close(unit)
  end function get_formatted_file_record_length

  function to_string_i4b(num, fmt)
    integer, intent(in) :: num
    character(len=*), intent(in), optional :: fmt
    character(len=:),allocatable :: to_string_i4b
    character(len=1024) :: tmp

    if(present(fmt)) then
       write(tmp,fmt) num
    else
       write(tmp,*) num
    endif
    to_string_i4b = trim(adjustl(tmp))
  end function to_string_i4b
end module util
