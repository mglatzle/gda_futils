module gda_futils
  use portability
  use util
#ifdef _OPENMP
  use omp_lib
#endif
  implicit none

  ! survival function x and y values
  real(fp), allocatable, dimension(:) :: sf_x, sf_y, delta_inv

contains

  subroutine compute_response( &
       n_sources, n_cells, &
       origin, &
       source_pos, &
       sigma_source_parallax, &
       tau_per_l, grid_shape, cell_size, &
       response, &
       n_threads, &
       error &
       )
    !*************************************************************************!
    !* Compute optical depth along lines of sight to a list of sources given *!
    !* an origin and a 3D density distribution on a Cartesian grid.          *!
    !*                                                                       *!
    !* All position are assumed to be given relative to the (0, 0, 0) corner *!
    !* of the grid with increasing first index corresponding to increasing x *!
    !* coordinate etc..                                                      *!
    !*                                                                       *!
    !* Integration along each LOS is performed from the origin up to the     *!
    !* source parallax (wrt. origin) minus three sigma or the grid edge. At  *!
    !* each cell crossing, if sigma is larger zero, a survival function is   *!
    !* evaluated by interpolation on the module arrays sf_x and sf_y. x, in  *!
    !* this case, is (1/r_source-1/r)/sigma, where r is the current radius   *!
    !* and r_source is the source radius.                                    *!
    !*                                                                       *!
    !* Parameters                                                            *!
    !* --------------------------------------------------------------------- *!
    !* n_sources : integer, intent(in): Number of sources.                   *!
    !*                                                                       *!
    !* n_cells : integer, intent(in): Number of cells.                       *!
    !*                                                                       *!
    !* origin : array, shape `(3)`, intent(in): The LOS origin.              *!
    !*                                                                       *!
    !* source_pos : array, shape `(3, n_sources)`, intent(in): Positions of  *!
    !*     the sources.                                                      *!
    !*                                                                       *!
    !* sigma_source_parallax : array, shape `(n_sources)`, intent(in):       *!
    !*     Absolute uncertainty on the parallaxes of the sources wrt. the    *!
    !*     origin.                                                           *!
    !*                                                                       *!
    !* tau_per_l : array, shape `(n_cells)`, intent(in): Optical path per    *!
    !*     physical length in the grid cells in C-order. Grid is assumed to  *!
    !*     have 3D shape `grid_shape`.                                       *!
    !*                                                                       *!
    !* grid_shape : array, shape `(3)`, intent(in): 3D shape of the grid.    *!
    !*                                                                       *!
    !* cell_size : float, intent(in): Physical linear cell size of the grid. *!
    !*                                                                       *!
    !* response : array, shape `(n_sources)`, intent(out): Optical depth     *!
    !*     integrated along each line of sight.                              *!
    !*                                                                       *!
    !* n_threads : integer, intent(in), optional: Number of OMP threads to   *!
    !*     use. Defaults to four if compiled with OMP support.               *!
    !*                                                                       *!
    !* error : integer, intent(out), optional: Non-zero if any errors are    *!
    !*     encountered.                                                      *!
    !*************************************************************************!

    ! in
    integer , intent(in) :: n_sources, n_cells
    real(fp), intent(in) :: origin(3)
    real(fp), intent(in) :: source_pos(3, n_sources)
    real(fp), intent(in) :: sigma_source_parallax(n_sources)
    real(fp), intent(in) :: tau_per_l(n_cells)
    integer , intent(in) :: grid_shape(3)
    real(fp), intent(in) :: cell_size

    ! inout
    real(fp), intent(inout) :: response(n_sources)

    ! optional
    integer, intent(in) , optional :: n_threads
    integer, intent(out), optional :: error

    ! local
    integer  :: j, curr, ijk(3), curr_sf_interp_idx, next_index, sgn(3)
    real(fp) :: path, total_path, pos(3), direction(3), source_parallax, &
         end_radius, survival_prob, delta_intercept(3), next_distance(3), &
         sigma_inv
    logical  :: end_radius_reached

#ifdef _OPENMP
    if(present(n_threads)) then
       call omp_set_num_threads(n_threads)
    else
       call omp_set_num_threads(4)
    end if
#endif

    response = 0

    !$omp parallel do default(none) &
    !$omp& private(j) &
    !$omp& shared(n_sources) &
    !$omp& private(pos, ijk, direction, source_parallax, end_radius) &
    !$omp& private(total_path, path, curr) &
    !$omp& private(survival_prob, curr_sf_interp_idx, end_radius_reached) &
    !$omp& private(delta_intercept, next_distance, sgn, next_index) &
    !$omp& private(sigma_inv) &
    !$omp& shared(tau_per_l, response, source_pos, sigma_source_parallax) &
    !$omp& shared(origin, grid_shape, cell_size, error)
    source_loop: do j=1, n_sources
       source_parallax = 1.0_fp/sqrt( &
            dot_product(source_pos(:, j)-origin, source_pos(:, j)-origin) &
            )
       end_radius = 1.0_fp / ( &
            source_parallax - 3*sigma_source_parallax(j) &
            ) / cell_size
       direction = source_parallax*(source_pos(:, j)-origin)
       pos = origin/cell_size
       ijk = ceiling(pos)
       total_path = 0
       curr_sf_interp_idx = 1
       end_radius_reached = .false.
       if(sigma_source_parallax(j) > 0) then
          sigma_inv = 1.0_fp/sigma_source_parallax(j)
       else
          sigma_inv = 0
       end if

       sgn = int(sign(1.0_fp, direction))
       delta_intercept = sgn/(direction + tiny(1.0_fp)*sgn)
       next_distance =  &
            sgn*(ijk - 0.5_fp*(1.0_fp-sgn) - pos)*delta_intercept

       cell_loop: do while(.true.)
          curr = ravel_index_3D_Cartesian_C_order(ijk, grid_shape)

          next_index = minloc(next_distance, dim=1)
          path = next_distance(next_index) - total_path
          ijk(next_index) = ijk(next_index) + sgn(next_index)
          next_distance(next_index) = next_distance(next_index) + &
               delta_intercept(next_index)

          ! end_radius reached within this cell?
          if(total_path+path >= end_radius) then
             path = end_radius - total_path
             end_radius_reached = .true.
          end if

          total_path = total_path + 0.5_fp*path
          call interpolate_survival_function( &
               total_path*cell_size, source_parallax, &
               sigma_inv, &
               curr_sf_interp_idx, survival_prob &
               )
          total_path = total_path + 0.5_fp*path

          ! update tau
          response(j) = response(j) + &
               path*cell_size*survival_prob*tau_per_l(curr)

          ! check loop termination conditions
          if( &
               end_radius_reached &
               .or. any(ijk < 1) &
               .or. any(ijk > grid_shape)) then
             exit cell_loop
          end if
       end do cell_loop
    end do source_loop
    !$omp end parallel do
  end subroutine compute_response

  subroutine compute_adjoint_response( &
       n_sources, n_cells, &
       origin, &
       source_pos, &
       sigma_source_parallax, &
       tau_per_l, grid_shape, cell_size, &
       response, &
       n_threads, &
       error &
       )
    !*************************************************************************!
    !* Compute density grid given optical depths along lines of sight to a   *!
    !* list of sources.                                                      *!
    !*                                                                       *!
    !* All position are assumed to be given relative to the (0, 0, 0) corner *!
    !* of the grid with increasing first index corresponding to increasing x *!
    !* coordinate etc..                                                      *!
    !*                                                                       *!
    !* Integration along each LOS is performed from the origin up to the     *!
    !* source parallax (wrt. origin) minus three sigma or the grid edge. At  *!
    !* each cell crossing, if sigma is larger zero, a survival function is   *!
    !* evaluated by interpolation on the module arrays sf_x and sf_y. x, in  *!
    !* this case, is (1/r_source-1/r)/sigma, where r is the current radius   *!
    !* and r_source is the source radius.                                    *!
    !*                                                                       *!
    !* Parameters                                                            *!
    !* --------------------------------------------------------------------- *!
    !* n_sources : integer, intent(in): Number of sources.                   *!
    !*                                                                       *!
    !* n_cells : integer, intent(in): Number of cells.                       *!
    !*                                                                       *!
    !* origin : array, shape `(3)`, intent(in): The LOS origin.              *!
    !*                                                                       *!
    !* source_pos : array, shape `(3, n_sources)`, intent(in): Positions of  *!
    !*     the sources.                                                      *!
    !*                                                                       *!
    !* sigma_source_parallax : array, shape `(n_sources)`, intent(in):       *!
    !*     Absolute uncertainty on the parallaxes of the sources wrt. the    *!
    !*     origin.                                                           *!
    !*                                                                       *!
    !* tau_per_l : array, shape `(n_cells)`, intent(in): Optical path per    *!
    !*     physical length in the grid cells in C-order. Grid is assumed to  *!
    !*     have 3D shape `grid_shape`.                                       *!
    !*                                                                       *!
    !* grid_shape : array, shape `(3)`, intent(in): 3D shape of the grid.    *!
    !*                                                                       *!
    !* cell_size : float, intent(in): Physical linear cell size of the grid. *!
    !*                                                                       *!
    !* response : array, shape `(n_sources)`, intent(in): Optical depth      *!
    !*     integrated along each line of sight.                              *!
    !*                                                                       *!
    !* n_threads : integer, intent(in), optional: Number of OMP threads to   *!
    !*     use. Defaults to four if compiled with OMP support.               *!
    !*                                                                       *!
    !* error : integer, intent(out), optional: Non-zero if any errors are    *!
    !*     encountered.                                                      *!
    !*************************************************************************!

    ! in
    integer , intent(in) :: n_sources, n_cells
    real(fp), intent(in) :: origin(3)
    real(fp), intent(in) :: source_pos(3, n_sources)
    real(fp), intent(in) :: sigma_source_parallax(n_sources)
    real(fp), intent(in) :: response(n_sources)
    integer , intent(in) :: grid_shape(3)
    real(fp), intent(in) :: cell_size

    ! inout
    real(fp), intent(inout) :: tau_per_l(n_cells)

    ! optional
    integer, intent(in) , optional :: n_threads
    integer, intent(out), optional :: error

    ! local
    integer  :: j, curr, ijk(3), curr_sf_interp_idx, next_index, sgn(3)
    real(fp) :: path, total_path, pos(3), direction(3), source_parallax, &
         end_radius, survival_prob, delta_intercept(3), next_distance(3), &
         sigma_inv
    logical  :: end_radius_reached

#ifdef _OPENMP
    if(present(n_threads)) then
       call omp_set_num_threads(n_threads)
    else
       call omp_set_num_threads(4)
    end if
#endif

    tau_per_l = 0

    !$omp parallel do default(none) &
    !$omp& private(j) &
    !$omp& shared(n_sources) &
    !$omp& private(pos, ijk, direction, source_parallax, end_radius) &
    !$omp& private(total_path, path, curr) &
    !$omp& private(survival_prob, curr_sf_interp_idx, end_radius_reached) &
    !$omp& private(delta_intercept, next_distance, sgn, next_index) &
    !$omp& private(sigma_inv) &
    !$omp& shared(tau_per_l, response, source_pos, sigma_source_parallax) &
    !$omp& shared(origin, grid_shape, cell_size, error)
    source_loop: do j=1, n_sources
       source_parallax = 1.0_fp/sqrt( &
            dot_product(source_pos(:, j)-origin, source_pos(:, j)-origin) &
            )
       end_radius = 1.0_fp / ( &
            source_parallax - 3*sigma_source_parallax(j) &
            ) / cell_size
       direction = source_parallax*(source_pos(:, j)-origin)
       pos = origin/cell_size
       ijk = ceiling(pos)
       total_path = 0
       curr_sf_interp_idx = 1
       end_radius_reached = .false.
       if(sigma_source_parallax(j) > 0) then
          sigma_inv = 1.0_fp/sigma_source_parallax(j)
       else
          sigma_inv = 0
       end if

       sgn = int(sign(1.0_fp, direction))
       delta_intercept = sgn/(direction + tiny(1.0_fp)*sgn)
       next_distance = &
            sgn*(ijk - 0.5_fp*(1.0_fp-sgn) - pos)*delta_intercept

       cell_loop: do while(.true.)
          curr = ravel_index_3D_Cartesian_C_order(ijk, grid_shape)

          next_index = minloc(next_distance, dim=1)
          path = next_distance(next_index) - total_path
          ijk(next_index) = ijk(next_index) + sgn(next_index)
          next_distance(next_index) = next_distance(next_index) + &
               delta_intercept(next_index)

          ! end_radius reached within this cell?
          if(total_path+path >= end_radius) then
             path = end_radius - total_path
             end_radius_reached = .true.
          end if

          total_path = total_path + 0.5_fp*path
          call interpolate_survival_function( &
               total_path*cell_size, source_parallax, &
               sigma_inv, &
               curr_sf_interp_idx, survival_prob &
               )
          total_path = total_path + 0.5_fp*path

          ! update tau_per_l
          !$omp atomic
          tau_per_l(curr) = tau_per_l(curr) + &
               path*cell_size*survival_prob*response(j)
          !$omp end atomic

          ! check loop termination conditions
          if( &
               end_radius_reached &
               .or. any(ijk < 1) &
               .or. any(ijk > grid_shape)) then
             exit cell_loop
          end if
       end do cell_loop
    end do source_loop
    !$omp end parallel do
  end subroutine compute_adjoint_response

  pure subroutine interpolate_survival_function( &
       r, source_parallax, sigma_inv, start_idx, prob &
       )
    real(fp), intent(in) :: r, source_parallax, sigma_inv

    integer, intent(inout) :: start_idx

    real(fp), intent(out) :: prob

    real(fp) :: x, frac

    if(sigma_inv > 0) then
       x = sigma_inv*(source_parallax - 1.0_fp/r)
       if(x < sf_x(1)) then
          prob = sf_y(1)
       else if(x > sf_x(size(sf_x))) then
          prob = sf_y(size(sf_y))
       else
          do while(sf_x(start_idx+1) < x)
             start_idx = start_idx+1
          end do
          frac = (x-sf_x(start_idx))*delta_inv(start_idx)
          prob = (1.0_fp-frac)*sf_y(start_idx) + frac*sf_y(start_idx+1)
       end if
    else
       prob = 1
    end if
  end subroutine interpolate_survival_function

  subroutine init_survival_function(n, x, y)
    integer , intent(in) :: n
    real(fp), intent(in) :: x(n), y(n)

    allocate(sf_x(n))
    allocate(sf_y(n))
    allocate(delta_inv(n-1))

    sf_x = x
    sf_y = y
    delta_inv = 1.0_fp/(sf_x(2:)-sf_x(:n-1))
  end subroutine init_survival_function

  subroutine terminate_survival_function
    deallocate(sf_x)
    deallocate(sf_y)
    deallocate(delta_inv)
  end subroutine terminate_survival_function
end module gda_futils
