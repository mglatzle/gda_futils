module portability
  use, intrinsic :: iso_fortran_env, only: REAL32, REAL64
  implicit none
  integer, parameter :: sp=REAL32
  integer, parameter :: dp=REAL64
  integer, parameter :: fp=dp
end module portability
