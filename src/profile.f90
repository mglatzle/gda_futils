program profile
  use gda_futils
  use util
  use portability
#ifdef _OPENMP
  use omp_lib
#endif
  implicit none

  real(fp) :: origin(3), cell_size
  integer :: grid_shape(3), n_sources, n_cells, n_threads, &
       n_vals_sf, error, unit
  real(fp), allocatable :: tau_per_l(:), source_pos(:, :), &
       sigma_source_parallax(:), response(:), source_radius(:), &
       x(:), y(:)
  character(:), allocatable :: which, file
  real(dp) :: start_time, end_time
  real(dp), allocatable :: buffer(:, :)
  integer :: j

  call get_cl_args(which, file, n_threads, grid_shape, n_vals_sf)

  ! initialization
  cell_size = 1
  error = 0
  n_cells = product(grid_shape)
  origin  = 0.5_fp*grid_shape*cell_size
  allocate(tau_per_l(n_cells))
  tau_per_l = 1
  n_sources = get_formatted_file_record_length(file)/8/3
  allocate(response(n_sources))
  allocate(source_pos(3, n_sources))
  allocate(buffer(3, n_sources))
  open(newunit=unit, file=file, status='old', action='read', &
       form='unformatted')
  read(unit) buffer
  close(unit)
  source_pos = real(buffer, fp)
  deallocate(buffer)

  allocate(sigma_source_parallax(n_sources))
  allocate(source_radius(n_sources))
  do j=1, n_sources
     source_radius(j) =  sqrt( &
          dot_product(source_pos(:, j)-origin, source_pos(:, j)-origin) &
          )
  end do
  sigma_source_parallax = 0.2_fp/source_radius

  allocate(x(n_vals_sf))
  allocate(y(n_vals_sf))
  x = [(-3._fp + (6._fp/n_vals_sf*j), j=1, n_vals_sf)]
  y = 1
  call init_survival_function(size(x), x, y)

  if(which == 'response') then
     write(*, *) "Profiling response:"
#ifdef _OPENMP
     start_time = omp_get_wtime()
#endif
     call compute_response(&
          n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
          tau_per_l, grid_shape, cell_size, response, n_threads, error &
          )
#ifdef _OPENMP
     end_time = omp_get_wtime()
     write(*, *) 'Threads, time[s]:', n_threads, end_time-start_time
#endif
     if(error /= 0) then
        write(*, *) 'Error:', error
        error stop 1
     end if

  else if(which == 'adjoint') then
     write(*, *) "Profiling adjoint response:"
#ifdef _OPENMP
     start_time = omp_get_wtime()
#endif
     call compute_adjoint_response( &
          n_sources, n_cells, origin, source_pos, sigma_source_parallax, &
          tau_per_l, grid_shape, cell_size, response, n_threads, error &
          )
#ifdef _OPENMP
     end_time = omp_get_wtime()
     write(*, *) 'Threads, time[s]:', n_threads, end_time-start_time
#endif
     if(error /= 0) then
        write(*, *) 'Error:', error
        error stop 1
     end if
  else
     write(*, *) 'Error, did not understand what to profile: '//which
     error stop 1
  end if

  call terminate_survival_function()
contains
  subroutine get_cl_args(which, file, n_threads, grid_shape, n_vals_sf)
    character(:), allocatable, intent(out) :: which, file
    integer, intent(out) :: n_threads, grid_shape(3), n_vals_sf

    integer arglen, stat, i
    character(:), allocatable :: buffer
    if(command_argument_count() /= 7) then
       write(* , *) &
            'Error, seven command-line arguments required as follows:'// &
            ' which_to_profile (response or adjoint)'// &
            ' path_to_source_pos_file'// &
            ' n_threads'// &
            ' grid_shape(1) grid_shape(2) grid_shape(3)'// &
            ' n_vals_sf'
       stop
    endif

    call get_command_argument(number=1, length=arglen)
    allocate(character(arglen) :: which)
    call get_command_argument(number=1, value=which, status=stat)

    call get_command_argument(number=2, length=arglen)
    allocate(character(arglen) :: file)
    call get_command_argument(number=2, value=file, status=stat)

    call get_command_argument(number=3, length=arglen)
    allocate(character(arglen) :: buffer)
    call get_command_argument(number=3, value=buffer, status=stat)
    read(buffer, *) n_threads
    deallocate(buffer)

    do i=1, 3
       call get_command_argument(number=3+i, length=arglen)
       allocate(character(arglen) :: buffer)
       call get_command_argument(number=3+1, value=buffer, status=stat)
       read(buffer, *) grid_shape(i)
       deallocate(buffer)
    end do

    call get_command_argument(number=7, length=arglen)
    allocate(character(arglen) :: buffer)
    call get_command_argument(number=7, value=buffer, status=stat)
    read(buffer, *) n_vals_sf
    deallocate(buffer)
  end subroutine get_cl_args
end program profile
