#!@Python3_EXECUTABLE@
import FRUIT
import os
import glob
from sys import argv
from argparse import ArgumentParser


src_dir = os.path.join("@CMAKE_SOURCE_DIR_NATIVE@", "src")
build_dir = "@CMAKE_BINARY_DIR_NATIVE@"
bin_dir = "@EXECUTABLE_OUTPUT_PATH@"
build_command = "@TEST_DRIVER_BUILD_CMD@"


def get_src_file_paths(files):
    if files == []:
        paths = glob.glob(
            os.path.join(src_dir, "**", "test_*.f90"),
            recursive=True
        )
    else:
        paths = []
        for f in files:
            paths.append(os.path.join(src_dir, f))
    return paths


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        'files', nargs='*',
        help="Fortran file(s) defining test modules."
        " Relative to main src dir. If none provided, run all tests."
    )

    p = get_src_file_paths(
        parser.parse_args(argv[1:]).files,
    )
    ts = FRUIT.test_suite(p)

    # cd to build folder to make sure build command will work
    os.chdir(build_dir)

    ts.build_run(
        os.path.join(
            src_dir,
            'tests',
            'driver_of_tests.f90'
        ),
        output_dir=bin_dir,
        build_command=build_command
    )
    ts.summary()
