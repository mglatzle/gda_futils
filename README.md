# GDA_futils
Fortran utilities for computation of Galactic Dust Absorption.

Part of the software stack used for dust reconstruction in Leike+
([2020](https://ui.adsabs.harvard.edu/abs/2020A%26A...639A.138L/abstract),
[2021](https://ui.adsabs.harvard.edu/abs/2020arXiv201114383L/abstract)).

## Shared library
A shared library that contains the main routines can be built using `cmake`:
- clone the repo
- `cd <repo_path>`
- `mkdir build && cd build && cmake .. && make`

### Dependencies
- `cmake > 3.13` and a corresponding build system tool (usually `make`)
- Fortran compiler
- `OpenMP` is required for parallelization.
- For Fortran unit testing:
  - Python3
  - [FRUITpy](https://github.com/acroucher/FRUITPy)

### Testing
Uses [FRUIT](https://sourceforge.net/projects/fortranxunit/) and
[FRUITpy](https://github.com/acroucher/FRUITPy) for Fortran unit testing. FRUIT
v. 3.4.4 is distributed along with the sources and built if Python3 and FRUITpy
dependencies are met.

To run tests from build directory:
- `cd bin`
- `./run_tests.py`

Paths of Fortran files containing FRUIT tests can optionally be supplied to
`run_tests.py`. The paths have to be relative to the `src` directory. E.g.:

```bash
./run_tests.py tests/test_util.f90
```

If paths are provided, only the respective tests are run, otherwise all tests
are run.

## `f2py`
A Python extension module can be built using `f2py`. Unfortunately, this is not
integrated into the `cmake` build configuration (yet).

To build the extension module, set the following in `f2py/makefile`:
- Your Python version (`PYVERSION`).
- Your Fortran compiler (`FC`, `FCF2PY`) and the compile flags (`FLAGS`,
  `F90FLAGS`). If you use `gfortran`, the defaults should work.
- The installation path (`LIBDIR`). By default, the module will be installed to
  your Python user site.

Then:
- `cd f2py`
- `make && make install`

Now you should be able to do

```python
from gda_futils import gda_futils
```

in a Python shell.

There are a few minimalistic tests in the `f2py` folder, to run them:

```bash
cd f2py && python test_response_adjoint.py && python test_adjointness.py
```

If no exceptions are raised, your module should be good (more extensive testing
is desirable, of course).

## License
With the exception of the files listed below, GPL v3.0 applies (see `LICENSE`
for details).

- `FRUIT/fruit.f90` is licensed under the 3-clause BSD license as follows:

  Copyright (c) 2005-2010, 2012-2013, Andrew Hang Chen and contributors,
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Neither the name of the Westinghouse Electric Company nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL ANDREW HANG CHEN AND CONTRIBUTORS BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Note:
  The orignal work of FRUIT was
  created by Andrew Hang Chen while working at Westinghouse Electric
  Company. The package was donated by Westinghouse Electric Company as
  an open source project.

  Contributors:
  Andrew Hang Chen
  sgould
  istomoya
  If you feel you should be listed here and aren't, please let us know.
